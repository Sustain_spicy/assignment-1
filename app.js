const http = require('http');
const https = require('https');
const url = require('url');
const config = require('./config');
const fs = require('fs');
const StringDecoder = require('string_decoder').StringDecoder;

var httpServer = http.createServer(function(req,res){
	unifiedServer(req,res);
});

var httpPort = config.httpPort;
httpServer.listen(httpPort, function(){
	console.log("Server Started on port "+ httpPort + " and on "+config.envName+" mode");
});


var httpsServerObject = {
	'key': fs.readFileSync('./https/key.pem'),
	'cert': fs.readFileSync('./https/cert.pem')
}
var httpsServer  = http.createServer(httpsServerObject,function(req,res){
	unifiedServer(req,res);
});

var httpsPort = config.httpsPort;
httpsServer.listen(httpsPort, function(){
	console.log("Server Started on port "+ httpsPort + " and on "+config.envName+" mode");
});

var unifiedServer = function(req,res){
	var parsedUrl = url.parse(req.url,true);
	var path = parsedUrl.pathname;
	var trimmedPath = path.replace(/^\/+|\/+$/g, '');

	var method = req.method;
	var headers = req.headers;

	var queryStringObject = parsedUrl.query;

	var decoder = new StringDecoder('utf-8');
	var buffer = '';

	req.on('data',function(data){
			buffer += decoder.write(data);
	});

	req.on('end', function(){
		buffer = decoder.end();

		var chosenHandler = typeof(routes[trimmedPath]) !== 'undefined' ? routes[trimmedPath] : handler.notFound; 
	
		var data = {
			'method':method,
			'trimmedPath':trimmedPath,
			'queryStringObject':queryStringObject,
			'headers':headers,
			'payLoad':buffer
		}

			

		chosenHandler(data, function(statusCode,payLoad){
			var statusCode = typeof(statusCode) == 'number' ? statusCode : 200;
			var payLoad = typeof(payLoad) == 'object' ? payLoad : {};

			var payLoadString = JSON.stringify(payLoad);

			res.setHeader('Content-Type', 'application/json');
			res.writeHead(statusCode);
			res.end(payLoadString);

		});
	});
}

var handler ={};

handler.hello = function(data,callback){
	callback(406,{'name':'hello welcome'});
}
handler.notFound = function(data,callback){
	callback(404);
}

var routes = {
	'hello':handler.hello
}

