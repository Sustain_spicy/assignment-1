
var environment = {};


environment.staging = {
	'httpPort': 3000,
	'httpsPort': 3001,
	'envName': 'staging'
}

environment.production = {
	'httpPort': 3000,
	'httpsPort': 3001,
	'envName': 'production'
}

var choosenEnv = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV : " ";

var envToPass = typeof(environment[choosenEnv]) == 'object' ? environment[choosenEnv] : environment.staging;

module.exports = envToPass;